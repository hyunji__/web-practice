// Declare express
const express = require('express');
const { render } = require('express/lib/response');

// Declare database and requitment
const JSONdb = require('simple-json-db');
const db = new JSONdb('database.json');
const db2 = new JSONdb('post.json');


// Declare port
const app = express();
const port = 3000;

// Set view engine
app.set('view engine', 'ejs');

// Set main page
app.get('/', (req, res) => {
    app.locals.title = db.get('title');
    app.locals.postUrl = db.get('posturl'); 
    app.locals.signtUrl = db.get('signurl');
    app.locals.content = db.get('content');

    app.locals.movie = db2.get('movie');
    app.locals.cc = db2.get('cc');
    app.locals.content1 = db2.get('content1');
    app.locals.date = db2.get('date');
    app.locals.ss = db2.get('ss');
    res.render('index');
})

// Set blog page
app.get('/post', (req, res) => {
    app.locals.title = db.get('title');
    app.locals.postUrl = db.get('posturl'); 
    app.locals.signtUrl = db.get('signurl');
    app.locals.content = db.get('content');

    app.locals.movie = db2.get('movie');
    app.locals.cc = db2.get('cc');
    app.locals.content1 = db2.get('content1');
    app.locals.date = db2.get('date');
    app.locals.ss = db2.get('ss');
    res.render('post');
});

//sign페이지
app.get('/sign', (req, res) => {
    app.locals.username = db.set('name', 'userinfo');
    


    res.render('sign');
});

app.get('/edt/:edturl', (req, res) => {
    edtUrl = req.params.edturl;
    app.locals.edtUrl = db2.get('edturl');
    app.locals.movie = db2.get('movie');
    app.locals.cc = db2.get('cc');
    app.locals.content1 = db2.get('content1');
    app.locals.date = db2.get('date');
    app.locals.ss = db2.get('ss');
    dbIndex = db2.get('edturl').indexOf(edtUrl);

    if (dbIndex != -1) {
        app.locals.movie = db2.get('movie')[dbIndex];
        app.locals.cc = db2.get('cc')[dbIndex];
        app.locals.date = db2.get('date')[dbIndex];
        app.locals.ss = db2.get('ss')[dbIndex];
        res.render('edt');
    } else {
        res.send('Page not found :(')
    }

    
})
app.get('/view', (req, res) => {
    app.locals.edtUrl = db2.get('edturl');
    app.locals.movie = db2.get('movie');
    app.locals.cc = db2.get('cc');
    app.locals.content1 = db2.get('content1');
    app.locals.date = db2.get('date');
    app.locals.ss = db2.get('ss');
    res.render('view');
});

app.get('/write', (req, res) => {
    app.locals.postUrl = db.get('posturl');
    app.locals.name = db.get('name');
    app.locals.movie = db2.get('movie');
    app.locals.ss = db2.get('ss');
    res.render('write');
});

app.get('/info', (req, res) => {
    app.locals.postUrl = db.get('posturl');
    app.locals.title = db.get('title');
    app.locals.name = db.get('name');
    app.locals.content = db.get('content');
    app.locals.movie = db2.get('movie');
    res.render("info");
});

// Run app
app.listen(port, () => {
    console.log('App is live');
});